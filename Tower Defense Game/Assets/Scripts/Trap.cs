﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    public EnableObject Enabler;
    // Use this for initialization
    void Start()
    {

    }

    void OnEnable()
    {
        StartCoroutine(Vanish());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider collision)
    {
        StartCoroutine(DamageEnemy(collision));

    }




    IEnumerator DamageEnemy(Collider collided)
    {
      
            Debug.Log("Finish");

            collided.gameObject.GetComponent<Health>().TakeDamage(1);
            yield return new WaitForSeconds(3);
          
        
    }

    IEnumerator Vanish()
    {
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
        Enabler.canClick = true;
    }
}