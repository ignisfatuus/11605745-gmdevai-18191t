﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableObject : MonoBehaviour {

    public GameObject Lava;
    public bool canClick;
	// Use this for initialization
	void Start ()
    {
        canClick = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (!canClick) return;
        if (Input.GetMouseButtonDown(0))
        {
            canClick = false;
            Lava.gameObject.SetActive(true);
        }

    }

    IEnumerator LavaCoolDown()
    {
        while (true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Lava.gameObject.SetActive(true);
            }

            yield return new WaitForSeconds(2);
        }
    }
}
