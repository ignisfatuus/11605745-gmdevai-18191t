﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour {


    public Projectile ProjectileToSpawn;
 
    public float RangeOfProjectile;
    public float ProjectileSpeed;
	// Use this for initialization
	void Start ()
    {

        
	}
	
	// Update is called once per frame
	void Update ()
    {
        ShootOnMousePosition();
	}

    void ShootOnMousePosition()
    {
        if (Input.GetMouseButtonDown(1))

        //Instantiate bullet on spawner's position
        {


            ProjectileToSpawn.transform.position = gameObject.transform.position;
       
            Instantiate(ProjectileToSpawn);

        }
 
        
        ProjectileToSpawn.hasBeenFired = true;
   
    }


}
